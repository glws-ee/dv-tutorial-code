#include "ExampleModuleColorPaint.h"

void ExampleModuleColorPaint::run() {
	auto events      = inputs.getEventInput("events").events();
	cv::Mat outFrame = cv::Mat::zeros(inputSize, CV_8UC3);

	for (const auto &event : events) {
		outFrame.at<cv::Vec3b>(event.y(), event.x()) = color;
	}

	outputs.getFrameOutput("frames") << outFrame << dv::commit;
}
