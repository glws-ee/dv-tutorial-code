#ifndef DV_EXAMPLE_MODULE_COLOR_PAINT_EXAMPLEMODULECOLORPAINT_H
#define DV_EXAMPLE_MODULE_COLOR_PAINT_EXAMPLEMODULECOLORPAINT_H

#include <dv-sdk/module.hpp>

#include <opencv2/imgproc.hpp>

class ExampleModuleColorPaint : public dv::ModuleBase {
private:
	cv::Size inputSize;
	cv::Vec3b color;

public:
	static void initInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events");
	}

	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addFrameOutput("frames");
	}

	static const char *initDescription() {
		return ("This example module renders all events to a frame in an adjustable color");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("red", dv::ConfigOption::intOption("Value of the red color component", 255, 0, 255));
		config.add("green", dv::ConfigOption::intOption("Value of the green color component", 255, 0, 255));
		config.add("blue", dv::ConfigOption::intOption("Value of the blue color component", 255, 0, 255));

		config.setPriorityOptions({"red", "green", "blue"});
	}

	ExampleModuleColorPaint() {
		outputs.getFrameOutput("frames").setup(inputs.getEventInput("events"));
		inputSize = inputs.getEventInput("events").size();
	}

	void configUpdate() override {
		color = cv::Vec3b(static_cast<uint8_t>(config.getInt("blue")), static_cast<uint8_t>(config.getInt("green")),
			static_cast<uint8_t>(config.getInt("red")));
	}

	void run() override;
};

registerModuleClass(ExampleModuleColorPaint)

#endif // DV_EXAMPLE_MODULE_COLOR_PAINT_EXAMPLEMODULECOLORPAINT_H
