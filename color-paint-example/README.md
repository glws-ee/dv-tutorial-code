# DV Example Color Paint Module

This is a simple module that has header / source separation
You can use this module as a starting point for your own module

## What does it do?
This module takes all events that arrive in a single `run` pass, and prints them onto a frame with a configurable color.

This module has three configuration knobs: *red*, *green* and *blue*. The setting of these knobs define the color that gets printed


## Getting started

To compile and install this module, run

```
cmake .
make
sudo make install
```


After this, you should be able to add the module to your DV configuration in the DV GUI.